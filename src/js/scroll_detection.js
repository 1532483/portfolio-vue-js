$(function () {
  var $win = $(window);

  $win.scroll(function () {
      if ($win.scrollTop() != 0){
        $('.s-menu').addClass('scrolled')
        $('.side-bar').addClass('scrolled')
      }else{
        $('.s-menu').removeClass('scrolled')
        $('.side-bar').removeClass('scrolled')
        $('.s-menu').removeClass('menu-open')
      }
  });
});