/**
 * Wait for the DOM to load to start accessing elements
 */
$(document).ready(function () {
  console.log('hey');
  //Set handlers
  $('div.image_project').click(expand);
  $('div.control').click(close);

  //Global var to clearInterval
  let interval;
  
  /**
   * Expands the creative work to show more information
   * and start a slideshow if there is more than 1 images
   * 
   * @param {DOMEvent} event 
   */
  function expand(event) {
    //Get node and expand
    let $node = event.delegateTarget;
    $($node).addClass('expanded');

    //Start Slideshow
    interval = startSlideshow($node);

    //Take off listener 
    $($node).off('click', expand);
  }

  /**
   * Close the expanded section and
   * interrupt the slideshow
   * 
   * @param {DOMEvent} event 
   */
  function close(event) {
    //Get node and close
    let $node = event.delegateTarget.parentNode;
    $($node).removeClass('expanded');

    //Stop Slideshow
    clearInterval(interval);

    //Hide slideshow images
    hideAll($node);

    //Put back listener
    setTimeout(function () {
      $($node).on('click', expand);
    }, 500);
  }


  /**
   * Hides all the images in secondary_images from the clicked item 
   * by setting the opacity to 0
   * 
   * @param {jQueryObject} $node 
   */
  function hideAll($node) {
    let images_list = $($($($node).children()[0]).children('.secondary_images')[0]).children('.image_container');

    $(images_list).css('opacity', '0');

  }


  /**
   * Start a slideshow by setInterval and validate list quantity > 1
   * 
   * 
   * @param {jQueryObject} $node 
   */
  function startSlideshow($node) {

    //Get Images List From Slideshow
    let images_list = $($($($node).children()[0]).children('.secondary_images')[0]).children('.image_container');

    //Counter
    let list_total = images_list.length;
    let current_count = 0;

    //Display first image
    setDisplay(current_count, '1', images_list)


    if (list_total > 1) {

      //Set interval
      interval = setInterval(function () {
        if (current_count === list_total - 1) {
          current_count = 0;
          setDisplay(list_total - 1, '0', images_list);
          setDisplay(current_count, '1', images_list);
        } else {
          current_count++;
          setDisplay(current_count - 1, '0', images_list);
          setDisplay(current_count, '1', images_list);
        }
      }, 2000);
    }

    return interval;
  }

  /**
   * Set opacity display the specified element in the list 
   * 
   * @param {int} count 
   * @param {String} display_type 
   * @param {HTMLAllCollection} list 
   */
  function setDisplay(count, display_type, list) {
    $(list[count]).css('opacity', display_type);
  }
})
