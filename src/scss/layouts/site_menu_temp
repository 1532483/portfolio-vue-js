//Menu
.s-menu,
.s-menu.menu-open
 {
  position: relative;
  overflow: hidden;
  color: $normal_theme_color;
  z-index: 100;

  //Menu Wrapper
  .s-menu-wrapper {

    //Menu Navigation
    .s-menu-nav {
      position: absolute;
      opacity: 0;
      height: 100px;
      top: calc(50% - 60px);
      left: 50%;

      // @media screen and (min-width: 1400px){
      //   top: auto;
      //   left: auto;
      //   height: auto;
      //   padding: 70px 85px 45px 0;
      // }

      //Navigation List
      ul {
        list-style: none;
        text-align: center;
        height: 100%;
        @include flexbox((display: flex,
          flex-direction: column,
          justify-content: flex-start));
        transition: transform .3s .3s, -webkit-transform .3s .3s;

        //List Item
        li {
          display: block;
          opacity: 0;
          text-align: center;
          transition: opacity .2s;

          //Link Within List Item
          a {
            display: inline-block;
            color: inherit;
            border-bottom: 2px solid transparent;
            font-weight: 600;
            letter-spacing: .5px;
            font-size: 24px;
            line-height: 55px;
            white-space: nowrap;
            transition: border-bottom .3s;
            transition: color .3s;

          }

          //Active - List Item
          &:active {
            border-bottom: 2px solid;
          }

          .router-link-exact-active {
            border-bottom: 2px solid;
          }
        }
      }
    }
  }


  .logo,
  .s-menu-ctrl {
    position: fixed;
    border: 2px solid;
    top: 30px;
    right: 30px;
    width: 45px;
    height: 45px;
    cursor: pointer;
    border-radius: 100%;

    @media screen and (min-width: 1024px) {
      top: 50px;
      right: 50px;
      width: 65px;
      height: 65px;
    }

    @media screen and (min-width: 1400px) {
      top: 60px;
      right: 60px;
      width: 75px;
      height: 75px;
    }
  }

  //Logo
  .logo {
    z-index: 98;
  }

  //Menu Controls
  .s-menu-ctrl {
    background: $normal_theme_color;
    z-index: 100;


    //Menu Button
    .s-menu-btn {

      //Hamburger Icon
      .hamburger,
      .hamburger:after,
      .hamburger:before {
        position: absolute;
        background-color: $dark_theme_color;
        width: 18px;
        height: 2px;
      }

      .hamburger {
        top: 50%;
        left: 50%;

        @include transform(translate(-50%, -50%));
        transition: background-color 0ms linear .2s;

        &:before,
        &:after {
          content: "";

          transition: top .2s ease-in .2s, bottom .2s ease-in .2s,
            transform .2s ease-out 0ms, -webkit-transform .2s ease-out 0ms;
        }

        &:before {
          top: -5px;
        }

        &:after {
          bottom: -5px;
        }
      }
    }
  }

  //Dark Theme & Menu Open
  &.dark-theme,
  &.menu-open {
    color: $dark_theme_color;
    border-bottom-color: $dark_theme_color;
  }

  //Menu Opened
  &.menu-open {
    position: fixed;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100vh;
    width: 100vw;

    //Menu Wrapper
    .s-menu-wrapper {
      position: absolute;
      background: $normal_theme_color;
      align-content: center;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;

      //Menu Navigation
      .s-menu-nav {
        opacity: 1;
        visibility: visible;
        @include transform(translate(-50%, 0));

        //Menu List
        ul {
          position: relative;
          list-style: none;
          text-align: center;
          height: 100%;
          @include flexbox((display: flex,
            flex-direction: column,
            justify-content: space-between));

          //Menu List Item
          li {
            opacity: 1;
          }
        }
      }
    }
  }
}

/* Media Queries */

//Large screens
@media only screen and (min-width: 1024px) {

  //Menu
  .s-menu,
  .s-menu.menu-open {
    position: fixed;
    top: 0;
    bottom: inherit;
    left: inherit;
    right: 0;
    height: 150px;
    transition: opacity .3s .3s, max-width .3s;

    .logo {
      display: block;
      opacity: 1;
      visibility: visible;
    }


    //Menu Wrapper
    .s-menu-wrapper {
      position: relative;
      overflow: hidden;

      //Menu Navigation
      .s-menu-nav {
        position: relative;
        overflow: hidden;
        visibility: visible;
        opacity: 1;
        background: transparent;
        top: auto;
        left: auto;
        height: auto;
        padding: 55px 55px 45px 0;

        //Navigation List
        ul {
          height: 100%;
          margin: 0 60px;
          @include flexbox((display: flex,
            flex-direction: row,
            align-content: center,
            align-items: center,
            justify-content: flex-start));

          //List Item
          li {
            display: block;
            opacity: 1;
            margin: 0 20px;
            @include flexbox((flex: 0 0 auto));
            transition-delay: 0ms !important;

            //Link Within List Item
            a {
              color: inherit;
              font-size: 18px;
              transition: color .2s, border-bottom .2s;

              &:hover {
                border-bottom: 2px solid;
              }
            }

            //Active - List Item
            &:active,
            .router-link-exact-active {
              border-bottom: 2px solid $normal_theme_color;
            }
          }
        }
      }
    }

    //Menu Controls
    .s-menu-ctrl {
      position: fixed;
      opacity: 0;
      visibility: hidden;
      transition: opacity .3s 0ms;
    }

    &.menu-open:not(.theme-black) {
      color: $normal_theme_color;
      border-bottom-color: $normal_theme_color;
    }    

    &.scrolled {
      transition: opacity .3s 0ms, max-width .3s;

      .s-menu-wrapper {
        transition: background 1s;
        background: transparent;

        ul {

          li {
            opacity: 0;

            a {
              color: white;
            }
          }
        }
      }

      .s-menu-ctrl {
        transition: opacity .3s;
        opacity: 1;
        visibility: visible;
      }

      &.menu-open {
        width: inherit;
        height: auto;
        background-color: $normal_theme_color;
        color: $dark_theme_color;
  
        left: inherit;
        bottom: inherit;
  
        .s-menu-wrapper {
          position: relative;
          background: transparent;
  
          .s-menu-nav {
            @include transform(none);
            position: relative;
            top: auto;
            left: auto;
            height: auto;
            overflow: hidden;
  
            ul{
  
              li{
                opacity: 1;
              }
            }
          }
        }
      }
    }
  }
}

@media only screen and (min-width: 1400px) {
  .s-menu,
  .s-menu.menu-open {
    .s-menu-wrapper {
      .s-menu-nav {
        top: auto;
        left: auto;
        height: auto;
        padding: 70px 85px 45px 0;

        ul {
          li {
            a {
              font-size: 24px;
            }
          }
        }
      }
    }
  }
}
