import Vue from 'vue'
import Router from 'vue-router'
import About from '@/components/About'
import Skills from '@/components/Skills'
import Portfolio from '@/components/Portfolio'
import Pdf from '@/components/pdf'
import Emoji from '@/components/Emoji'
import Email from '@/components/Email'
import Stock from '@/components/Stock'
import Pacman from '@/components/Pacman'
import Music from '@/components/Music'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'about',
      component: About
    },
    {
      path: '/skills',
      name: 'skills',
      component: Skills
    }, 
    {
      path: '/portfolio',
      name: 'portfolio',
      component: Portfolio
    },
    {
      path: '/cv',
      name: 'pdf',
      component: Pdf
    },
    {
      path: '/portfolio/emoji',
      name: 'emoji',
      component: Emoji
    },
    {
      path: '/portfolio/email',
      name: 'email',
      component: Email
    },
    {
      path: '/portfolio/stock',
      name: 'stock',
      component: Stock
    },
    {
      path: '/portfolio/pacman',
      name: 'pacman',
      component: Pacman
    },
    {
      path: '/portfolio/music',
      name: 'music',
      component: Music
    }

  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
